import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcTo;
import javafx.scene.shape.Circle;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;

/**
 * Demo program to show a bug in path rendering. The bug was first detected when displaying
 * airspaces (CTRs) over a map. When you reach a certain zoom level the airspace
 * boundaries are not rendered correctly anymore. I can see these errors on my MacBook Pro Retina
 * with JDK 8u76 for scales >1024. To run the demo just start the program and zoom in and out with
 * the mouse wheel.
 * 
 * https://bugs.openjdk.java.net/browse/JDK-8160599
 * https://bugs.openjdk.java.net/browse/JDK-8160600
 */
public class ShapeOutlineBug2 extends Application {
	
	private static enum ShapeType {CIRCLE, POLYGON}
	
	private final static ShapeType SHAPE_TYPE = ShapeType.POLYGON;
	private final static boolean USE_PATH = true;
	private final static boolean USE_DASH = true;
	
	private final static double SIZE = 900;
	private final static double D = 0.5*SIZE;
	private final static double sqrt2 = Math.sqrt(2);
	
	private Pane root;
	
	private Shape shape;
	
	private DoubleProperty scale = new SimpleDoubleProperty(1.0);

	private DoubleProperty radius = new SimpleDoubleProperty(1.0);

	@Override
	public void start(Stage stage) throws Exception {
		radius.bind(scale.multiply(D));
		
		root = new Pane();
		
		Label scaleLabel = new Label();
		scaleLabel.setLayoutX(10);
		scaleLabel.setLayoutY(10);
		scaleLabel.textProperty().bind(scale.asString().concat(" : Scale"));
		
		Label radiusLabel = new Label();
		radiusLabel.setLayoutX(10);
		radiusLabel.setLayoutY(30);
		radiusLabel.textProperty().bind(radius.asString().concat(" : Radius"));
		
		Circle fixPoint = new Circle(D, D, 6);
		
		createShape();

		root.getChildren().addAll(shape, fixPoint, scaleLabel, radiusLabel);
		
		stage.setScene(new Scene(root, SIZE, SIZE));
		stage.show();
		
		updateShape();
		
		root.setOnScroll(new EventHandler<ScrollEvent>() {
			@Override
			public void handle(ScrollEvent event) {
				if (event.getDeltaY() > 0) {
					scale.set(scale.get()*2);
				} else {
					scale.set(scale.get()/2);
				}
				updateShape();
			}});		
	}
	
	private void createShape() {
		if (USE_PATH) {
			shape = new Path();
		} else {
			if (SHAPE_TYPE == ShapeType.CIRCLE) {
				shape = new Circle();
			} else if (SHAPE_TYPE == ShapeType.POLYGON) {
				shape = new Polygon();
			}
		}
		shape.setFill(Color.RED.darker().deriveColor(0, 1, 1, 0.1));
		shape.setStroke(Color.BLUE.darker());
		shape.setStrokeWidth(2.0);
		if (USE_DASH) {
			shape.getStrokeDashArray().addAll(10.0, 9.0);
		}
	}
	
	private void updateShape() {
		double r = radius.get();
		double cx = D - r/sqrt2;
		double cy = D - r/sqrt2;
		
		if (USE_PATH) {
			Path path = (Path)shape;
			path.getElements().clear();
			if (SHAPE_TYPE == ShapeType.CIRCLE) {
				path.getElements().add(new MoveTo(cx, cy));
				path.getElements().add(new LineTo(cx, cy + r));
				path.getElements().add(new ArcTo(r, r, -90, cx + r, cy, false, false));
			} else if (SHAPE_TYPE == ShapeType.POLYGON) {
				path.getElements().add(new MoveTo(cx, cy));
				double size = r/Math.sqrt(2);
				path.getElements().add(new LineTo(cx + size, cy + size));
				path.getElements().add(new LineTo(cx + 2*size, cy));
				path.getElements().add(new LineTo(cx + size, cy - size));
				path.getElements().add(new LineTo(cx, cy));				
			}
			path.getElements().add(new ClosePath());
		} else {
			if (SHAPE_TYPE == ShapeType.CIRCLE) {
				Circle circle = (Circle)shape;
				circle.setCenterX(cx);
				circle.setCenterY(cy);
				circle.setRadius(r);
			} else if (SHAPE_TYPE == ShapeType.POLYGON) {
				Polygon polygon = (Polygon)shape;
				polygon.getPoints().clear();
				double size = r/Math.sqrt(2);
				polygon.getPoints().addAll(cx, cy);
				polygon.getPoints().addAll(cx + size, cy + size);
				polygon.getPoints().addAll(cx + 2*size, cy);
				polygon.getPoints().addAll(cx + size, cy - size);
				polygon.getPoints().addAll(cx, cy);
			}
		}

	}

	public static void main(String[] args) {
		Application.launch(args);
	}
}
