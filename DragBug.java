import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class DragBug extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Pane dragPane = new Pane() {
			@Override
			protected void layoutChildren() {
			}
		};
		Rectangle rectangle = new Rectangle(50, 50);
		rectangle.setMouseTransparent(true);
		rectangle.relocate(375, 275);
		dragPane.getChildren().add(rectangle);
		dragPane.addEventHandler(ScrollEvent.SCROLL, event -> rectangle.relocate(rectangle.getLayoutX() + event.getDeltaX(), rectangle.getLayoutY() + event.getDeltaY()));
		stage.setWidth(800);
		stage.setHeight(600);
		stage.setScene(new Scene(dragPane));
		stage.show();
	}
}